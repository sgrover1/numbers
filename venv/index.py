from flask import Flask, redirect, request
atomic_numbers = {1:'Hydrogen',2:'Helium',3:
                  'Lithium',
                  4:'Beryllium',
                  5:'Boron',
                  6: 'Carbon',
                  7: 'Nitrogen',
                  8:'Oxygen',
                  9: 'Fluorine',
                  10: 'Neon',
                  11: 'Sodium',
                  12: 'Magnesium',
                  13: 'Aluminium',
                  14:'Silicon',
                  15:'Phosphorus',
                  16:'Sulfur',
                  17:'Chlorine',
                  18:'Argon',
                  19:'Potassium',
                  20:'Calcium',
                  21:'Scandium',
                  22:'Titanium',
                  23:'Vandium',
                  24:'Chromium',
                  25:'Manganese',
                  26:'Iron',
                  27:'Cobalt',
                  28:'Nickel',
                  29:'Copper',
                  30:'Zinc',
                  31:'Galium',
                  32:'Germanium',
                  33:'Arsenic',
                  34:'Selenium',
                  35:'Bromine',
                  36:'Krypton',
                  }
def isprime(n):
    '''check if integer n is a prime'''
    # make sure n is a positive integer
    n = abs(int(n))
    # 0 and 1 are not primes
    if n < 2:
        return False
    # 2 is the only even prime number
    if n == 2:
        return True
    # all other even numbers are not primes
    if not n & 1:
        return False
    # range starts with 3 and only needs to go up the squareroot of n
    # for all odd numbers
    for x in range(3, int(n**0.5)+1, 2):
        if n % x == 0:
            return False
    return True

fib = lambda n: fib(int(n) - 1) + fib(int(n) - 2) if n > 2 else 1
binary = lambda n: bin(int(n))
atomic4 = lambda n: atomic_numbers[n] if n <= 103 and n!=0 else ''
triangular = lambda n: True if n in [0, 1, 3, 6, 10, 15, 21, 28, 36, 45, 55, 66, 78, 91, 105, 120,136, 153, 171, 190, 210, 231, 253, 276, 300, 325, 351, 378, 406, 435, 465, 496, 528, 561, 595, 630, 666, 703, 741, 780, 820, 861, 903, 946, 990] else False
def facts4(num):
    try:
        num = int(num)
    except ValueError:
        return f'{num} is not an integer!<br>' if num != '' else 'blank is not an integer!<br>'
    if num > 1000:
        return 'Enter A Number From 1 to 1000!'
    facts = []
    facts += '<span>&#8226;</span> {} in binary is {},'.format(num, binary(num))
    facts += f'<span>&#8226;</span> {num} in octal is {oct(num)},'
    if num <= len(atomic_numbers):
        facts += f'<span>&#8226;</span> {num} is the atomic number for {atomic4(num)}, '
    if isprime(num) == True:
        facts += f'<span>&#8226;</span> {num} is a prime number, '
    if isprime(num) != True:
        facts += f'<span>&#8226;</span> {num} is not a prime number, '
    if triangular(num) == True:
        facts += f'<span>&#8226;</span> {num} is a triangular number, '
    if triangular(num) == False:
        facts += f'<span>&#8226;</span> {num} is not a triangular number, '
    if num%2==0 and num!=0: facts+=f'<span>&#8226;</span> {num} is an even number'
    if num%2!=0 and num!=0: facts+=f'<span>&#8226;</span> {num} is an odd number'

    return facts

app = Flask(__name__)

@app.route('/')
def index():
    return f'''
    <html>
        <head>
            <title>NumberFacts</title>
        </head>
        <body>
            <h1>Learn Cool Facts About Numbers!</h1>
            <form action="/n", method="post"> 
                <label for="lname">Please enter a number from 1 to 1000:</label>
                <input type="text" id="lname" name="lname"><br><br>
                <input type="submit" value="Submit">
            </form>
            <iframe src="https://giphy.com/embed/3o6YfTUIfDYjPdnk52" width="480" height="480" frameBorder="0" class="giphy-embed" allowFullScreen></iframe><p><a href="https://giphy.com/gifs/animation-illustration-red-3o6YfTUIfDYjPdnk52"></a></p>
        </body>
    </html>
    '''
@app.route('/n', methods=['GET','POST'])
def num():
    s = ""
    for item in facts4(request.form['lname']):
        if item == ",":
            s += '<br>'
        else:
            s += '{}'.format(item)
    return f'''{s} <br><br> <a href="/">Check Another Number<a><br><img src="https://media.beliefnet.com/~/media/photos-with-attribution/entertainment/numbers.jpg?as=1" alt="numbers", style="width:100px;height:150px;">'''
if __name__ == '__main__' :
    app.run()